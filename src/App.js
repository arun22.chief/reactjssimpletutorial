import logo from './logo.svg';
import './App.css';
import Register from './UserManagement/Register';
import RegistrationForm from './UserManagement/RegistrationForm';
import Header from './Layout/Header';
import Footer from './Layout/Footer';

function App() {
  const UserInfo = {
    "Name": "Arun",
    "Age": 30
  }
  const myClick = function (Name) {
    alert(Name);
    secondAlert();
  }
  const secondAlert = function(){
    alert("Second Alert");
  }

  return (
    <div>
      <Header></Header>
      <RegistrationForm></RegistrationForm>
      {/* <button onClick={() => myClick(UserInfo.Age)} >Alert Button</button>
      <Register color="red" myButton={myClick} userinfo={UserInfo} /> */}
      <Footer></Footer>
    </div>
  );

}

export default App;
