import React, { useState } from 'react';

class RegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { userName: '', age: null }
    }

    updateName = (event) => {
        this.setState({ userName: event.target.value });
    }

    updateAge = (event) => {
        this.setState({ age: event.target.value });
    }

    mySubmitHandler = (event) => {
        event.preventDefault();
        alert("You are submitting " + this.state.userName);
    }



    render() {
        const mystyle = {
            color: "white",
            backgroundColor: "DodgerBlue",
            padding: "10px",
            fontFamily: "Arial"
        };
        return (
            <form onSubmit={this.mySubmitHandler}>
                <div style={mystyle}>{this.state.userName}</div>  <br></br>
                <input type="text" onChange={this.updateName} ></input>
                <br></br>
                {this.state.age} <br></br>
                <input type="number" onChange={this.updateAge} ></input> <br></br><br></br>
                <input type="submit" onClick={this.mySubmitHandler}></input><br></br><br></br>
            </form>
        )
    }
}

export default RegistrationForm;